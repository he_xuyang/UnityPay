using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityPay.Core.PayBase;
using UnityPay.Core.BankClass;
using UnityPay.Core;
using UnityPay.Core.DTO;

namespace UnityPay.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 工商银行参数
            String APP_ID = "工银e支付AppId";
            String MY_PRIVATE_KEY = @"工商银行私钥，一个商户一般只有一个私钥";
            String APIGW_PUBLIC_KEY = @"工商银行网关公钥，分测试和正式";
            string merid = "商户号，一个门店对应一个";
            #endregion

            #region 建设银行参数
            string pubKey = "建设银行商户公钥，一个门店对应一个";
            string posid = "柜台号，一般一个收银机对应一个";
            string MERCHANTID = "商户号";
            string BRANCHID = "建设银行分行号";
            string IP = "127.0.0.1";//建设银行外联平台所在的服务器IP
            int PROT = 9999;//建设银行外联平台所使用的端口号
            string USER_ID = "外联平台操作员ID";
            string PASSWORD = "外联平台登录密码";

            #endregion

            //工商银行
            BankPayActionInterface icbc = new ICBC(APP_ID,MY_PRIVATE_KEY,APIGW_PUBLIC_KEY,merid);
            //建设银行
            BankPayActionInterface ccb = new CCB(pubKey, BRANCHID,MERCHANTID,posid,IP, PROT,"");//给相关参数填好
            var data = icbc.Pay("0.1", "134620711104177171","202110201501","");
            
            var data1 = icbc.Query("202110201501", "");

            var data2 = icbc.IcbcRefund("202110201501", "0.1","test2021124");

            var data3 = icbc.IcbcRefundQuery("202110201501", "test20120927005");
            Console.Read();
        }
    }
}
