﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnityPay.Core.DTO
{
    /// <summary>
    /// 建设工商银行通用返回dto
    /// 支付请求返回的消息
    /// </summary>
    public class BankMsgDTO
    {
        /// <summary>
        /// 订单结果 Y:支付成功
        /// U：不确定
        /// Q：需要轮询
        /// </summary>
        public string RESULT { get; set; }
        /// <summary>
        /// 二维码类型
        /// </summary>
        public string QRCODETYPE { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string ORDERID { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public string AMOUNT { get; set; }
        /// <summary>
        /// 等待时间，单位秒
        /// </summary>
        public string WAITTIME { get; set; }
        /// <summary>
        /// 建行流水号
        /// </summary>
        public string TRACEID { get; set; }
        /// <summary>
        /// 错误码
        /// </summary>
        public string ERRCODE { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ERRMSG { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string SIGN { get; set; }
        /// <summary>
        /// 支付宝交易号
        /// PAY102
        /// </summary>
        public string ZFB_NO { get; set; }
        /// <summary>
        /// 支付宝交易状态
        /// 交易状态：WAIT_BUYER_PAY（交易创建，等待买家付款）TRADE_CLOSED（未付款交易超时关闭，或支付完成后全额退款）TRADE_SUCCESS（交易支付成功）TRADE_FINISHED（交易结束，不可退款）
        /// </summary>
        public string ZFB_STATE { get; set; }
        /// <summary>
        /// 微信支付订单号
        /// </summary>
        public string WECHAT_NO { get; set; }
        /// <summary>
        /// SUCCESS—支付成功REFUND—转入退款NOTPAY—未支付CLOSED—已关闭REVOKED—已撤销（刷卡支付）USERPAYING--用户支付中PAYERROR--支付失败（其他原因，如银行返回失败）
        /// </summary>
        public string WECHAT_STATE { get; set; }
        /// <summary>
        /// 是否需要继续调用订单关闭交易Y：需要N：不需要
        /// </summary>
        public string RECALL { get; set; }


    }
}
