﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UnityPay.Core.Utils
{
    public class SocketUtil
    {
        private string _ip = string.Empty;
        private int _port = 0;
        public Socket _socket = null;
        public byte[] buffer = new byte[1024];
        object _lock = new object();
        public event EventHandler ReceiveMessageEvent;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ip">连接服务器的IP</param>
        /// <param name="port">连接服务器的端口</param>
        public SocketUtil(string ip, int port)
        {
            this._ip = ip;
            this._port = port;
        }
        public SocketUtil(int port)
        {
            this._ip = "127.0.0.1";
            this._port = port;
        }
        public Socket GetSocket()
        {
            return _socket;
        }
        /// <summary>
        /// 开启服务,连接服务端
        /// </summary>
        public void StartClient()
        {
            try
            {
                //1.0 实例化套接字(IP4寻址地址,流式传输,TCP协议)
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //2.0 创建IP对象
                IPAddress address = IPAddress.Parse(_ip);
                //3.0 创建网络端口包括ip和端口
                IPEndPoint endPoint = new IPEndPoint(address, _port);
                //4.0 建立连接
                _socket.Connect(endPoint);

                //5.0接受数据
                //Thread thread = new Thread(ReceiveMessage);

                //thread.Start(_socket);
            }
            catch (Exception ex)
            {
                //_socket.Shutdown(SocketShutdown.Both);
                //_socket.Close();
            }
        }
        /// <summary>
        /// 向服务器发送byte
        /// </summary>
        /// <param name="data"></param>
        public void SendMessage(byte[] data)
        {
            try
            {
                _socket.Send(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        /// <summary>
        /// 接受服务器消息
        /// </summary>
        public byte[] ReceiveMessage(object socket)
        {
            Socket ServiceScoket = (Socket)socket;
            byte[] data;
            try
            {
                
                while (true)
                {
                    try
                    {
                        lock (_lock)
                        {
                            //写入缓冲
                            int lengit = ServiceScoket.Receive(buffer);

                            if (lengit > 0)
                            {
                                data = new byte[lengit];
                                Array.Copy(buffer,data,lengit);
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }


                }

            }
            catch (Exception ex)
            {
                data = new byte[1];
                //ServiceScoket.Shutdown(SocketShutdown.Both);
                //ServiceScoket.Close();
            }
            return data;
        }

    }
}
