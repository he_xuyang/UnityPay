﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;

namespace UnityPay.Core.Utils
{
    public class HttpUtil
    {
        /// <summary>
        /// 默认使用POST
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string HttpSend(string url,Method method= Method.POST)
        {
            var client = new RestClient(url);

            var req = new RestRequest(method);
            
            IRestResponse res = client.Execute(req);

            string data = res.Content;

            return data;
        }

    }
}
