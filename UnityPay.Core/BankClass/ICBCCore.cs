﻿using sdk_cop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UnityPay.Core.BankClass
{
    /// <summary>
    /// 工商银行支付核心类
    /// </summary>
    public class ICBCCore
    {
        public ICBCCore()
        {
            Console.WriteLine("工商银行SDK中的核心代码");
        }

    }
    /// <summary>
    /// 二维码支付
    /// </summary>
    public class QrcodePayRequestV2 : AbstractIcbcRequest<QrcodePayResponseV2>//QrcodePayResponseV2
    {
        public QrcodePayRequestV2()
        {
            //正式的接口
            this.setServiceUrl("https://gw.open.icbc.com.cn/api/mybank/pay/qrcode/scanned/pay/V2");
        }
        public override Type getBizContentClass()
        {
            return Type.GetType("UnityPay.Core.BankClass" + ".QrcodePayRequestV2+QrcodePayRequestV2Biz", true, true);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override Type getResponseClass()
        {
            return Type.GetType("UnityPay.Core.BankClass.QrcodePayResponseV2");
        }

        public override bool isNeedEncrypt()
        {
            return false;
        }
        [DataContract]
        public class QrcodePayRequestV2Biz : BizContent
        {
            [DataMember]
            private String qr_code;
            [DataMember]
            private String mer_id;
            [DataMember]
            private String out_trade_no;
            [DataMember]
            private String order_amt;
            [DataMember]
            private String trade_date;
            [DataMember]
            private String trade_time;
            [DataMember]
            private String attach;

            public String getMerId()
            {
                return mer_id;
            }
            public void setMerId(String value)
            {
                mer_id = value;
            }

            public String getQrCode()
            {
                return qr_code;
            }
            public void setQrCode(String value)
            {
                qr_code = value;
            }

            public String getOutTradeNo()
            {
                return out_trade_no;
            }
            public void setOutTradeNo(String value)
            {
                out_trade_no = value;
            }

            public String getOrderAmt()
            {
                return order_amt;
            }
            public void setOrderAmt(String value)
            {
                order_amt = value;
            }

            public String getTradeDate()
            {
                return trade_date;
            }
            public void setTradeDate(String value)
            {
                trade_date = value;
            }

            public String getTradeTime()
            {
                return trade_time;
            }
            public void setTradeTime(String value)
            {
                trade_time = value;
            }

            public String getAttach()
            {
                return attach;
            }
            public void setAttach(String value)
            {
                attach = value;
            }
        }
    }
    /// <summary>
    /// 二维码支付响应
    /// </summary>
    [Serializable]
    [DataContract]
    public class QrcodePayResponseV2 : IcbcResponse
    {
        // 交易结果标志，0：支付中，1：支付成功，2：支付失败
        [DataMember]
        public String pay_status;
        //用户标识
        [DataMember]
        public String cust_id;

        // 付款卡号
        [DataMember]
        public String card_no;

        // 订单总金额
        [DataMember]
        public String total_amt;

        // 积分抵扣金额
        [DataMember]
        public String point_amt;

        [DataMember]
        public String ecoupon_amt;

        // 优惠立减金额（商户部分）
        [DataMember]
        public String mer_disc_amt;

        // 优惠券金额
        [DataMember]
        public String coupon_amt;

        // 银行补贴金额
        [DataMember]
        public String bank_disc_amt;

        // 实际支付金额
        [DataMember]
        private String payment_amt;

        // 商户订单号
        [DataMember]
        private String out_trade_no;

        // 行内订单号
        [DataMember]
        private String order_id;

        // 支付完成时间
        [DataMember]
        private String pay_time;

        // 总优惠金额
        [DataMember]
        private String total_disc_amt;

        // 发卡行名称
        [DataMember]
        private String bank_name;

        // 渠道标识
        [DataMember]
        private String channel;

        // 商户附加信息
        [DataMember]
        private String attach;

        // 第三方支付机构的客户编号
        [DataMember]
        private String tp_cust_id;
        [DataMember]
        public String tp_order_id;
        // 工行交易检索号
        [DataMember]
        private String trx_ser_no;

        public String getPayStatus()
        {
            return pay_status;
        }

        public void setPayStatus(String payStatus)
        {
            this.pay_status = payStatus;
        }

        public String getCustId()
        {
            return cust_id;
        }

        public void setCustId(String custId)
        {
            this.cust_id = custId;
        }

        public String getCardNo()
        {
            return card_no;
        }

        public void setCardNo(String cardNo)
        {
            this.card_no = cardNo;
        }

        public String getTotalAmt()
        {
            return total_amt;
        }

        public void setTotalAmt(String totalAmt)
        {
            this.total_amt = totalAmt;
        }

        public String getPointAmt()
        {
            return point_amt;
        }

        public void setPointAmt(String pointAmt)
        {
            this.point_amt = pointAmt;
        }

        public String getEcouponAmt()
        {
            return ecoupon_amt;
        }

        public void setEcouponAmt(String ecouponAmt)
        {
            this.ecoupon_amt = ecouponAmt;
        }

        public String getMerDiscAmt()
        {
            return mer_disc_amt;
        }

        public void setMerDiscAmt(String merDiscAmt)
        {
            this.mer_disc_amt = merDiscAmt;
        }

        public String getCouponAmt()
        {
            return coupon_amt;
        }

        public void setCouponAmt(String couponAmt)
        {
            this.coupon_amt = couponAmt;
        }

        public String getBankDiscAmt()
        {
            return bank_disc_amt;
        }

        public void setBankDiscAmt(String bankDiscAmt)
        {
            this.bank_disc_amt = bankDiscAmt;
        }

        public String getPaymentAmt()
        {
            return payment_amt;
        }

        public void setPaymentAmt(String paymentAmt)
        {
            this.payment_amt = paymentAmt;
        }

        public String getOutTradeNo()
        {
            return out_trade_no;
        }

        public void setOutTradeNo(String outTradeNo)
        {
            this.out_trade_no = outTradeNo;
        }

        public String getOrderId()
        {
            return order_id;
        }

        public void setOrderId(String orderId)
        {
            this.order_id = orderId;
        }

        public String getPayTime()
        {
            return pay_time;
        }

        public void setPayTime(String payTime)
        {
            this.pay_time = payTime;
        }

        public String getTotalDiscAmt()
        {
            return total_disc_amt;
        }

        public void setTotalDiscAmt(String totalDiscAmt)
        {
            this.total_disc_amt = totalDiscAmt;
        }

        public String getBankName()
        {
            return bank_name;
        }

        public void setBankName(String bankName)
        {
            this.bank_name = bankName;
        }

        public String getChannel()
        {
            return channel;
        }

        public void setChannel(String channel)
        {
            this.channel = channel;
        }

        public String getAttach()
        {
            return attach;
        }

        public void setAttach(String attach)
        {
            this.attach = attach;
        }

        public String getTpCustId()
        {
            return tp_cust_id;
        }

        public void setTpCustId(String tpCustId)
        {
            this.tp_cust_id = tpCustId;
        }
        public String getTrxSerNo()
        {
            return trx_ser_no;
        }

        public void setTrxSerNo(String trxSerNo)
        {
            this.trx_ser_no = trxSerNo;
        }
        public string getTpOrderId()
        {
            return this.tp_order_id;
        }
    }

    /// <summary>
    /// 工商银行二维码交易状态查询
    /// </summary>
    public class QrcodePayQueryRequestV2 : AbstractIcbcRequest<QrcodePayQueryResponseV2>
    {
        public override Type getBizContentClass()
        {
            return Type.GetType("UnityPay.Core.BankClass.QrcodePayQueryRequestV2+QrcodeQueryRequestV2Biz", true, true);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override Type getResponseClass()
        {
            return Type.GetType("UnityPay.Core.BankClass.QrcodePayQueryResponseV2");
        }

        public override bool isNeedEncrypt()
        {
            return false;
        }

        public class QrcodeQueryRequestV2Biz : BizContent
        {
            [DataMember]
            public String mer_id;
            [DataMember]
            public String out_trade_no;
            [DataMember]
            public String order_id;
            [DataMember]
            public String cust_id;
            public String getMerId()
            {
                return mer_id;
            }
            public void setMerId(String value)
            {
                mer_id = value;
            }

            public String getOutTradeNo()
            {
                return out_trade_no;
            }
            public void setOutTradeNo(String value)
            {
                out_trade_no = value;
            }

            public String getOrderId()
            {
                return order_id;
            }
            public void setOrderId(String value)
            {
                order_id = value;
            }

            public String getCustId()
            {
                return cust_id;
            }
            public void setCustId(String value)
            {
                cust_id = value;
            }
        }
    }

    /// <summary>
    /// 二维码支付查询响应
    /// </summary>
    [Serializable]
    [DataContract]
    public class QrcodePayQueryResponseV2 : IcbcResponse
    {
        // 交易结果标志，0：支付中，1：支付成功，2：支付失败
        [DataMember]
        private String pay_status;
        //用户标识
        [DataMember]
        private String cust_id;

        // 付款卡号
        [DataMember]
        private String card_no;

        // 订单总金额
        [DataMember]
        private String total_amt;

        // 积分抵扣金额
        [DataMember]
        private String point_amt;

        [DataMember]
        private String ecoupon_amt;

        // 优惠立减金额（商户部分）
        [DataMember]
        private String mer_disc_amt;

        // 优惠券金额
        [DataMember]
        private String coupon_amt;

        // 银行补贴金额
        [DataMember]
        private String bank_disc_amt;

        // 实际支付金额
        [DataMember]
        private String payment_amt;

        // 商户订单号
        [DataMember]
        private String out_trade_no;

        // 行内订单号
        [DataMember]
        private String order_id;

        // 支付完成时间
        [DataMember]
        private String pay_time;

        // 总优惠金额
        [DataMember]
        private String total_disc_amt;

        // 发卡行名称
        [DataMember]
        private String bank_name;

        // 渠道标识
        [DataMember]
        private String channel;

        // 商户附加信息
        [DataMember]
        private String attach;

        // 第三方支付机构的客户编号
        [DataMember]
        private String tp_cust_id;

        // 工行交易检索号
        [DataMember]
        private String trx_ser_no;
        
        // 第三方支付订单
        [DataMember]
        private string tp_order_id;

        public String getTpOrderId()
        {
            return this.tp_order_id;
        }
        public void setTpOrderId(String tpOrderid)
        {
            this.tp_order_id = tpOrderid;
        }
        public String getPayStatus()
        {
            return pay_status;
        }

        public void setPayStatus(String payStatus)
        {
            this.pay_status = payStatus;
        }

        public String getCustId()
        {
            return cust_id;
        }

        public void setCustId(String custId)
        {
            this.cust_id = custId;
        }

        public String getCardNo()
        {
            return card_no;
        }

        public void setCardNo(String cardNo)
        {
            this.card_no = cardNo;
        }

        public String getTotalAmt()
        {
            return total_amt;
        }

        public void setTotalAmt(String totalAmt)
        {
            this.total_amt = totalAmt;
        }

        public String getPointAmt()
        {
            return point_amt;
        }

        public void setPointAmt(String pointAmt)
        {
            this.point_amt = pointAmt;
        }

        public String getEcouponAmt()
        {
            return ecoupon_amt;
        }

        public void setEcouponAmt(String ecouponAmt)
        {
            this.ecoupon_amt = ecouponAmt;
        }

        public String getMerDiscAmt()
        {
            return mer_disc_amt;
        }

        public void setMerDiscAmt(String merDiscAmt)
        {
            this.mer_disc_amt = merDiscAmt;
        }

        public String getCouponAmt()
        {
            return coupon_amt;
        }

        public void setCouponAmt(String couponAmt)
        {
            this.coupon_amt = couponAmt;
        }

        public String getBankDiscAmt()
        {
            return bank_disc_amt;
        }

        public void setBankDiscAmt(String bankDiscAmt)
        {
            this.bank_disc_amt = bankDiscAmt;
        }

        public String getPaymentAmt()
        {
            return payment_amt;
        }

        public void setPaymentAmt(String paymentAmt)
        {
            this.payment_amt = paymentAmt;
        }

        public String getOutTradeNo()
        {
            return out_trade_no;
        }

        public void setOutTradeNo(String outTradeNo)
        {
            this.out_trade_no = outTradeNo;
        }

        public String getOrderId()
        {
            return order_id;
        }

        public void setOrderId(String orderId)
        {
            this.order_id = orderId;
        }

        public String getPayTime()
        {
            return pay_time;
        }

        public void setPayTime(String payTime)
        {
            this.pay_time = payTime;
        }

        public String getTotalDiscAmt()
        {
            return total_disc_amt;
        }

        public void setTotalDiscAmt(String totalDiscAmt)
        {
            this.total_disc_amt = totalDiscAmt;
        }

        public String getBankName()
        {
            return bank_name;
        }

        public void setBankName(String bankName)
        {
            this.bank_name = bankName;
        }

        public String getChannel()
        {
            return channel;
        }

        public void setChannel(String channel)
        {
            this.channel = channel;
        }

        public String getAttach()
        {
            return attach;
        }

        public void setAttach(String attach)
        {
            this.attach = attach;
        }

        public String getTpCustId()
        {
            return tp_cust_id;
        }

        public void setTpCustId(String tpCustId)
        {
            this.tp_cust_id = tpCustId;
        }

        public String getTrxSerNo()
        {
            return trx_ser_no;
        }

        public void setTrxSerNo(String trxSerNo)
        {
            this.trx_ser_no = trxSerNo;
        }
    }
    /// <summary>
    /// 工商银行退货请求
    /// </summary>
    public class QrcodePayReturnRequestV2 : AbstractIcbcRequest<QrcodePayReturnResponseV2>//QrcodePayResponseV2
    {
        public QrcodePayReturnRequestV2()
        {
            //正式URL
            this.setServiceUrl("https://gw.open.icbc.com.cn/api/mybank/pay/qrcode/scanned/return/V2");
        }
        public override Type getBizContentClass()
        {
            return Type.GetType("UnityPay.Core.BankClass" + ".QrcodePayReturnRequestV2+QrcodePayReturnRequestV2Biz", true, true);
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override Type getResponseClass()
        {
            return Type.GetType("UnityPay.Core.BankClass.QrcodePayReturnResponseV2");
        }

        public override bool isNeedEncrypt()
        {
            return false;
        }
        public class QrcodePayReturnRequestV2Biz : BizContent
        {
            public string cust_id;
            public string mer_id;
            public string out_trade_no;
            public string order_id;
            public string reject_no;
            public string reject_amt;
            public string oper_id;
            public string merattach;

            public String getMerId()
            {
                return mer_id;
            }
            public void setMerId(String value)
            {
                mer_id = value;
            }

            public String getOutTradeNo()
            {
                return out_trade_no;
            }
            public void setOutTradeNo(String value)
            {
                out_trade_no = value;
            }

            public String getOrderId()
            {
                return order_id;
            }
            public void setOrderId(String value)
            {
                order_id = value;
            }

            public String getCustId()
            {
                return cust_id;
            }
            public void setCustId(String value)
            {
                cust_id = value;
            }

            public String getRejectNo()
            {
                return reject_no;
            }
            public void setRejectNo(String value)
            {
                reject_no = value;
            }

            public String getRejectAmt()
            {
                return reject_amt;
            }
            public void setRejectAmt(String value)
            {
                reject_amt = value;
            }

            public String getMerattach()
            {
                return merattach;
            }
            public void setMerattach(String value)
            {
                merattach = value;
            }

            public String getOperId()
            {
                return oper_id;
            }
            public void setOperId(String value)
            {
                oper_id = value;
            }

        }
    }
    /// <summary>
    /// 工商银行退货响应
    /// </summary>
    [Serializable]
    [DataContract]
    public class QrcodePayReturnResponseV2 : IcbcResponse
    {
        //商户订单号
        [DataMember]
        private String out_trade_no;

        //行内订单号
        [DataMember]
        private String order_id;

        //退款编号
        [DataMember]
        private String reject_no;

        //实退金额
        [DataMember]
        private String real_reject_amt;

        //本次退货总金额
        [DataMember]
        private String reject_amt;

        //积分退货金额
        [DataMember]
        private String reject_point;

        //电子券退货金额
        [DataMember]
        private String reject_ecoupon;

        //交易卡号
        [DataMember]
        private String card_no;

        //本次所退优惠立减金额（商户部分）
        [DataMember]
        private String reject_mer_disc_amt;

        //本次所退银行补贴金额
        [DataMember]
        private String reject_bank_disc_amt;

        //本次所退总优惠金额
        [DataMember]
        private String reject_total_disc_amt;

        //客户编号
        [DataMember]
        private String cust_id;

        //交易检索号
        [DataMember]
        private String trace_no;

        //交易渠道
        [DataMember]
        public String order_channel;

        public String getOutTradeNo()
        {
            return out_trade_no;
        }

        public void setOutTradeNo(String outTradeNo)
        {
            this.out_trade_no = outTradeNo;
        }

        public String getOrderId()
        {
            return order_id;
        }

        public void setOrderId(String orderId)
        {
            this.order_id = orderId;
        }

        public String getRejectNo()
        {
            return reject_no;
        }

        public void setRejectNo(String rejectNo)
        {
            this.reject_no = rejectNo;
        }

        public String getRealRejectAmt()
        {
            return real_reject_amt;
        }

        public void setRealRejectAmt(String realRejectAmt)
        {
            this.real_reject_amt = realRejectAmt;
        }

        public String getRejectAmt()
        {
            return reject_amt;
        }

        public void setRejectAmt(String rejectAmt)
        {
            this.reject_amt = rejectAmt;
        }

        public String getRejectPoint()
        {
            return reject_point;
        }

        public void setRejectPoint(String rejectPoint)
        {
            this.reject_point = rejectPoint;
        }

        public String getRejectEcoupon()
        {
            return reject_ecoupon;
        }

        public void setRejectEcoupon(String rejectEcoupon)
        {
            this.reject_ecoupon = rejectEcoupon;
        }

        public String getCardNo()
        {
            return card_no;
        }

        public void setCardNo(String cardNo)
        {
            this.card_no = cardNo;
        }

        public String getRejectMerDiscAmt()
        {
            return reject_mer_disc_amt;
        }

        public void setRejectMerDiscAmt(String rejectMerDiscAmt)
        {
            this.reject_mer_disc_amt = rejectMerDiscAmt;
        }

        public String getRejectBankDiscAmt()
        {
            return reject_bank_disc_amt;
        }

        public void setRejectBankDiscAmt(String rejectBankDiscAmt)
        {
            this.reject_bank_disc_amt = rejectBankDiscAmt;
        }

        public String getRejectTotalDiscAmt()
        {
            return reject_total_disc_amt;
        }

        public void setRejectTotalDiscAmt(String rejectTotalDiscAmt)
        {
            this.reject_total_disc_amt = rejectTotalDiscAmt;
        }

        public String getCustId()
        {
            return cust_id;
        }

        public void setCustId(String custId)
        {
            this.cust_id = custId;
        }

        public String getTraceNo()
        {
            return trace_no;
        }

        public void setTraceNo(String traceNo)
        {
            this.trace_no = traceNo;
        }

        public String getOrderChannel()
        {
            return order_channel;
        }

        public void setOrderChannel(String orderChannel)
        {
            this.order_channel = orderChannel;
        }


    }

    /// <summary>
    /// 工商银行二维码退货状态查询
    /// </summary>
    public class QrcodePayReturnQueryRequestV2 : AbstractIcbcRequest<QrcodePayReturnQueryResponseV2>
    {
        public override Type getBizContentClass()
        {
            return Type.GetType("UnityPay.Core.BankClass.QrcodePayReturnQueryRequestV2+QrcodePayReturnQueryRequestV2Biz");
        }

        public override string getMethod()
        {
            return "POST";
        }

        public override Type getResponseClass()
        {
            return Type.GetType("UnityPay.Core.BankClass.QrcodePayReturnQueryResponseV2");
        }

        public override bool isNeedEncrypt()
        {
            return false;
        }
        public class QrcodePayReturnQueryRequestV2Biz : BizContent
        {
            [DataMember]
            public String mer_id;
            [DataMember]
            public String out_trade_no;
            [DataMember]
            public String order_id;
            [DataMember]
            public String cust_id;
            [DataMember]
            public String reject_no;

            public String getMerId()
            {
                return mer_id;
            }
            public void setMerId(String value)
            {
                mer_id = value;
            }

            public String getOutTradeNo()
            {
                return out_trade_no;
            }
            public void setOutTradeNo(String value)
            {
                out_trade_no = value;
            }

            public String getOrderId()
            {
                return order_id;
            }
            public void setOrderId(String value)
            {
                order_id = value;
            }

            public String getCustId()
            {
                return cust_id;
            }
            public void setCustId(String value)
            {
                cust_id = value;
            }

            public String getRejectNo()
            {
                return reject_no;
            }
            public void setRejectNo(String value)
            {
                reject_no = value;
            }
        }

    }

    /// <summary>
    /// 工商银行退货状态查询响应
    /// </summary>
    public class QrcodePayReturnQueryResponseV2 : IcbcResponse
    {
        // 商户系统订单号
        [DataMember]
        private String out_trade_no;

        // 行内系统订单号
        [DataMember]
        private String order_id;

        // 退款数据集合
        [DataMember]
        private String refund_json_list;
        [DataMember]
        public string real_reject_amt;
        [DataMember]
        public string order_channel;
        public void setRealRejectAmt(string rejectAmt)
        {
            this.real_reject_amt = rejectAmt;
        }
        public string getRealRejectAmt()
        {
            return this.real_reject_amt;
        }
        public void setChannel(string value)
        {
            this.order_channel = value;
        }
        public string getChannel()
        {
            return order_channel;
        }
        public String getOutTradeNo()
        {
            return out_trade_no;
        }

        public void setOutTradeNo(String outTradeNo)
        {
            this.out_trade_no = outTradeNo;
        }

        public String getOrderId()
        {
            return order_id;
        }

        public void setOrderId(String orderId)
        {
            this.order_id = orderId;
        }

        public String getRefundJsonList()
        {
            return refund_json_list;
        }

        public void setRefundJsonList(String refund_json_list)
        {
            this.refund_json_list = refund_json_list;
        }
    }


}
