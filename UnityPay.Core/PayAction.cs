﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityPay.Core.PayBase;
using UnityPay.Core.BankClass;
using UnityPay.Core.DTO;

namespace UnityPay.Core
{
    public class PayAction: BankPayActionInterface
    {
        private BankPayActionInterface pay;

        /// <summary>
        /// 建设银行
        /// </summary>
        /// <param name="BRANCHID"></param>
        /// <param name="MERCHANTID"></param>
        /// <param name="POSID"></param>
        /// <param name="pubKey"></param>
        /// <param name="IP"></param>
        /// <param name="port"></param>
        public PayAction(string BRANCHID,string MERCHANTID,string POSID,string pubKey,string IP="127.0.0.1",int port=9991,string bankName= "CCB")
        {
            if (bankName.ToUpper()=="CCB")
            {
                pay = new CCBPay(BRANCHID, MERCHANTID, POSID, pubKey, IP, port);
            }
            else
            {
                throw new Exception("请提供建设银行必要的信息参数");
            }
        }
        /// <summary>
        /// 工商银行
        /// </summary>
        /// <param name="appid">工商银行商户appid</param>
        /// <param name="priKey">工商银行商户私钥</param>
        /// <param name="apiwg">工商银行网关公钥</param>
        /// <param name="merid">商户号</param>
        public PayAction(string appid, string priKey, string apiwg, string merid,string bankName)
        {
            if (bankName.ToUpper()=="ICBC")
            {
                pay = new ICBC(appid, priKey, apiwg, merid);
            }
            else
            {
                throw new Exception("请提供工商银行必要的信息参数");
            }
        }
        public DTO.BankMsgDTO Query(string orderid, string qrcodetype)
        {
            return pay.Query(orderid, qrcodetype);
        }
        public DTO.BankMsgDTO ZfbWxOrderClose(string orderid, string qrcodetype)
        {
            return pay.ZfbWxOrderClose(orderid,qrcodetype);
        }

        public DTO.BankMsgDTO ZfbWxQuery(string orderid, string qrcodetype)
        {
            return pay.ZfbWxQuery(orderid, qrcodetype);
        }

        public DTO.BankMsgDTO Pay(string amount, string qrcode, string orderid, string goodsname)
        {
            return pay.Pay(amount, qrcode, orderid, goodsname);
        }
        public string Refund(string CUST_ID, string USER_ID, string PASSWORD, string orderid, string amount, string Refundid)
        {
            return pay.Refund(CUST_ID,USER_ID,PASSWORD,orderid,amount,Refundid);
        }
        public BankMsgDTO IcbcRefund(string orderid, string amount, string Refundid)
        {
            return pay.IcbcRefund(orderid, amount, Refundid);
        }

        public BankMsgDTO IcbcRefundQuery(string orderid, string Refundid)
        {
            return pay.IcbcRefundQuery(orderid, Refundid);
        }
    }
}
