﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityPay.Core.BankClass;
using UnityPay.Core.DTO;

namespace UnityPay.Core
{
    /// <summary>
    /// 银行支付动作
    /// </summary>
    public interface BankPayActionInterface
    {
        /// <summary>
        /// 付款接口
        /// </summary>
        /// <param name="amount">支付金额</param>
        /// <param name="qrcode">二维码</param>
        /// <param name="orderid">系统订单号</param>
        /// <param name="goodsname">商品名,可空</param>
        /// <returns></returns>
        BankMsgDTO Pay(string amount, string qrcode, string orderid,  string goodsname);
        /// <summary>
        /// 付款查询接口
        /// </summary>
        /// <param name="orderid">系统订单号</param>
        /// <param name="qrcodetype">二维码类型</param>
        /// <returns></returns>
        BankMsgDTO Query(string orderid, string qrcodetype);
        /// <summary>
        /// 支付宝微信支付查询
        /// </summary>
        /// <param name="orderid">订单号</param>
        /// <param name="qrcodetype">二维码类型</param>
        /// <returns></returns>
        BankMsgDTO ZfbWxQuery(string orderid, string qrcodetype);
        /// <summary>
        /// 支付宝微信订单关闭
        /// </summary>
        /// <param name="orderid">系统交易订单</param>
        /// <param name="qrcodetype">二维码类型</param>
        /// <returns></returns>
        BankMsgDTO ZfbWxOrderClose(string orderid, string qrcodetype);
        /// <summary>
        /// 建设银行退款
        /// </summary>
        /// <param name="CUST_ID">商户ID</param>
        /// <param name="USER_ID">操作员号</param>
        /// <param name="PASSWORD">操作员密码</param>
        /// <param name="orderid">系统订单号</param>
        /// <param name="amount">退款金额</param>
        /// <param name="Refundid">退款号，由系统生成可不填</param>
        /// <returns></returns>
        string Refund(string CUST_ID, string USER_ID, string PASSWORD, string orderid, string amount, string Refundid);
        /// <summary>
        /// 工商银行退款接口
        /// </summary>
        /// <param name="orderid">系统订单号</param>
        /// <param name="amount">退款金额</param>
        /// <param name="Refundid">退款编号</param>
        /// <returns></returns>
        BankMsgDTO IcbcRefund(string orderid,string amount, string Refundid);
        /// <summary>
        /// 工商银行退款查询接口
        /// </summary>
        /// <param name="orderid">订单号</param>
        /// <param name="Refundid">退单号</param>
        /// <returns></returns>
        BankMsgDTO IcbcRefundQuery(string orderid,string Refundid);
    }
}
