﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnityPay.Core.PayBase
{
    /// <summary>
    /// 银行基类
    /// 目前只有建设银行再用
    /// </summary>
    public abstract class BankBaseClass
    {
        /// <summary>
        /// 工商银行AppId
        /// </summary>
        protected string APP_ID { get; set; }
        /// <summary>
        /// 建设银行公钥、工商银行网关公钥
        /// </summary>
        protected string PubKey { get; set; }
        /// <summary>
        /// 工商银行私钥
        /// </summary>
        protected string PriKey { get; set; }
        /// <summary>
        /// 工商银行加密类型
        /// </summary>
        protected string SIGNTYPE { get; set; }
        /// <summary>
        /// 请求链接
        /// </summary>
        protected string Url { get; set; }
        /// <summary>
        /// 请求API地址
        /// </summary>
        protected string UrlAction { get; set; }
        /// <summary>
        /// 商户号
        /// 必输入
        /// </summary>
        public string MERCHANTID { get; set; }
        /// <summary>
        /// 获取请求链接
        /// </summary>
        /// <returns></returns>
        public abstract string GetUrl();
    }
}
